######################################################################################
###                                                                                ###
###   LAHKA - NDABIAN - HOSTMAN - medical center manager application               ###
###                                                                                ###
######################################################################################
 
CRUD APPLICATION BACKEND SETUP:
-------------------------------
:1--> create data model :2--> install the language database migration process 
:3--> create the queries using sqlc to generate the CRUD setter/getter methods
:5--> integrate the unit test for each entity

STEP:1--> DATA MODELLING:
- use the vuerd or any oder DBML tool to modulate and create the ERM Datamodel 
- generate the SQL script 

STEP:2--> DATABASE MIGRATION - GOLANG_MIGRATE: 
- setup golang-migrate a database migration tool (do the same for any other languages)
- A-Installation:
    //set repository    
     curl -s https://packagecloud.io/install/repositories/golang-migrate/migrate/script.deb.sh | sudo bash
    sudo apt-get update
    sudo apt-get install migrate

- B- Create empty initial database schema
    run:$ migrate create -ext sql -dir database/migration -seq init_schema
    test the two generated sql scripts for database up migration and down-migration

STEP:3--> DATABASE OBJECT MAPPING - SQLC : 
- install SQLC an sql database mapping utility using brew any other tool or as a docker 
container
    A- used: sudo snap install sqlc on ubuntu
    B- use:   sqlc help or sqlc version
    C- -run: sqlc init to generate an empty sqlc.yml file
        Do that in your project data directory
        f.e.: cd ~/go/src/patient/databas/sqlc/

        sample sqlc files:
       ------------sqlc.yml--------------
        version: "2"
        sql:
            - engine:    "postgresql"
            queries:    "query.sql"
            schema:     "schema.sql"
        gen:
            go:
                package: "db"
                out: "../"
                sql_package: "pgx/v5"
        -------------------------------------

        sample query file:
        ----------------query.sql-----------
        -- name: GetUsers :many
        SELECT
            id,
            full_name,
            email,
            created_at,
            contry_code
        FROM
            users;
        -------------------------------------
 
        sample schema file:
        ----------------schema.sql----------
        CREATE TABLE "tasks" (
            "id"          serial   PRIMARY KEY,
            "title"       varchar NOT NULL,
            "description" text,
            "completed"   boolean NOT NULL
        );

        CREATE TABLE "users" (
            "id"        serial   PRIMARY KEY,
            "full_name" varchar NOT NULL,
            "email"     varchar,
            "created_at"  timestamptz NOT NULL DEFAULT  NOW(),
            "contry_code"   serial
        ); 
        -------------------------------------------

STEP:4-->  CRUD UNIT TEST - GOLANG TESTING/TESTIFY 




GENERAL REMARKS
====================================================

GOLANG:
    convention:  in Golang, test file and related code file are placed in the same folder f.e: (account.go, test_account.go)