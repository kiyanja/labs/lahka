include ./default.env

jambo:
	@echo "Hello LAHKA"

dbstop: 
	@echo "\n Stopping the Database!!! \n"
	sudo docker-compose -f /var/share/workspace/work/db/postgresql/14/docker-compose.yml down
dbstart: 
	@echo "\n Starting the Database!!! \n"
	sudo docker-compose -f /var/share/workspace/work/db/postgresql/14/docker-compose.yml up -d
dbconn:
	@echo "\n connecting to the database remote container via psql!! \n"
	sudo docker exec -it $$DBCONTAINER psql -U postgres

dbcreate:
	@echo "\n creating the database in remote container !! \n"
	sudo docker exec -it $$DBCONTAINER createdb --username=$$DBUSER $$DBNAME
dbdrop:
	@echo "\n dropping the database in remote container !! \n"
	sudo docker exec -it $$DBCONTAINER dropdb --username=$$DBUSER $$DBNAME

dbinitmigrate:
	@echo "\n initialize database migration in remote container !! \n"
	 migrate create -ext sql -dir database/migration -seq init_schema    
dbfixmigrate:
	@echo "\n initFixialize database migration in remote container !! \n"
	migrate -path database/migration/ -database "postgresql://postgres:postgres@localhost:5432/$$DBNAME?sslmode=disable" force $$VERSION
dbmigrateup:
	@echo "\n migrating up the database schema in remote container !! \n"
	migrate -path database/migration -database "postgresql://postgres:postgres@localhost:5432/$$DBNAME?sslmode=disable" -verbose up
dbmigratedown:
	@echo "\n migrating down the database schema in remote container !! \n"
	migrate -path database/migration -database "postgresql://postgres:postgres@localhost:5432/$$DBNAME?sslmode=disable" -verbose down

.dbreset:
	@echo "\n reseting database schema migration in remote container !! \n"
	cd database/migration, dbmigratedown, dbmigrateup 

sqlcgen:
	@echo "\n generating sqlc code for data models !! \n"
	cd database/sqlc/; sqlc generate

run:
	@echo "\n running the main application !! \n"
	go run main.go
serve:
	@echo "\n spin up LAHKA application server!! \n"