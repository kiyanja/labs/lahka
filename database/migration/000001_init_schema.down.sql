/*
Title: schema-down
Description: Database initialisation script
Author: Benaloga
Last_Update: January 2024
Remarks:
This script is manually updated
Please make sure to respect the following order to garante the erasal of all tables
this is due to some table not being erased due to dependencies
ORDER: personnels,customers,insurances,users,
       services, service_items, 
       account_entries, accounts_transfers, accounts, 
       appointments, 
       patient_records, patients
*/

DROP TABLE IF EXISTS personnels;

DROP TABLE IF EXISTS insurances;

DROP TABLE IF EXISTS services;
DROP TABLE IF EXISTS service_items;
DROP TABLE IF EXISTS customers;

DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS contacts;


DROP TABLE IF EXISTS account_entries;
DROP TABLE IF EXISTS account_transfers;
DROP TABLE IF EXISTS accounts;

DROP TABLE IF EXISTS appointments;

DROP TABLE IF EXISTS patient_records;
DROP TABLE IF EXISTS patients;