
        
CREATE TABLE account_entries
(
  id         bigserial   NOT NULL,
  account_id bigserial   NOT NULL,
  amount     bigint     ,
  created_at timestamptz DEFAULT now(),
  PRIMARY KEY (id)
);

CREATE TABLE account_transfers
(
  id              bigserial   NOT NULL,
  from_account_id bigserial   NOT NULL,
  to_account_id   bigserial   NOT NULL,
  amount          bigint      NOT NULL,
  created_at      timestamptz NOT NULL DEFAULT now(),
  PRIMARY KEY (id)
);

CREATE TABLE accounts
(
  id         bigserial   NOT NULL,
  owner      varchar     NOT NULL,
  balance    bigint      NOT NULL,
  currency   varchar     NOT NULL,
  created_at timestamptz NOT NULL DEFAULT now(),
  PRIMARY KEY (id)
);

COMMENT ON TABLE accounts IS 'customer billing account';

CREATE TABLE appointments
(
  id         bigserial   NOT NULL,
  title      varchar     NOT NULL,
  schedule   date        NOT NULL,
  status     varchar     NOT NULL,
  updated_at timestamptz NOT NULL DEFAULT now(),
  patient_id bigserial   NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE contacts
(
  id      bigserial NOT NULL,
  email   varchar   NOT NULL DEFAULT 'newcontact@kiyanja.org',
  phone   bigint    NOT NULL DEFAULT 23711111111,
  street  varchar   DEFAULT 'empty',
  postbox varchar   NOT NULL DEFAULT 'empty',
  PRIMARY KEY (id)
);

CREATE TABLE customers
(
  id         bigserial NOT NULL,
  name       varchar   NOT NULL,
  patient_id bigserial NOT NULL,
  user_id    bigserial NOT NULL,
  account_id bigserial NOT NULL,
  PRIMARY KEY (id)
);

COMMENT ON TABLE customers IS 'billable users';

CREATE TABLE insurances
(
  id         bigserial NOT NULL,
  name       varchar   NOT NULL,
  code       varchar   NOT NULL,
  patient_id bigserial NOT NULL,
  PRIMARY KEY (id)
);

COMMENT ON TABLE insurances IS 'medicalinsurance company';

CREATE TABLE patient_records
(
  id         bigserial   NOT NULL,
  title      varchar     NOT NULL,
  created_at timestamptz NOT NULL,
  anamnesis  text        NOT NULL,
  patient_id bigserial   NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE patients
(
  id           bigserial NOT NULL,
  code         varchar   NOT NULL,
  name         varchar   NOT NULL,
  gender       varchar  ,
  birthday     date      NOT NULL,
  email        varchar   DEFAULT 'NA',
  phone        varchar   NOT NULL,
  country_code int       NOT NULL DEFAULT 237,
  PRIMARY KEY (id)
);

CREATE TABLE personnels
(
  id         bigserial NOT NULL,
  name       varchar   NOT NULL,
  email      varchar   NOT NULL DEFAULT 'newstaff@kiyanja.cm',
  profession varchar   NOT NULL,
  department varchar   NOT NULL,
  service_id bigserial NOT NULL,
  user_id    bigserial NOT NULL,
  PRIMARY KEY (id)
);

COMMENT ON TABLE personnels IS 'staff members';

CREATE TABLE service_items
(
  id    bigserial NOT NULL,
  name  varchar   NOT NULL,
  code  varchar   NOT NULL,
  price bigint   ,
  PRIMARY KEY (id)
);

CREATE TABLE services
(
  id              bigserial   NOT NULL,
  title           varchar     NOT NULL,
  detail          text        NOT NULL,
  created_at      timestamptz NOT NULL DEFAULT now(),
  service_item_id bigserial   NOT NULL,
  appointment_id  bigserial   NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE users
(
  id         bigserial NOT NULL,
  name       varchar   NOT NULL,
  login      varchar   NOT NULL,
  password   varchar   DEFAULT '',
  status     boolean   DEFAULT false,
  contact_id bigserial NOT NULL,
  PRIMARY KEY (id)
);

COMMENT ON TABLE users IS 'system users';

ALTER TABLE patient_records
  ADD CONSTRAINT FK_patients_TO_patient_records
    FOREIGN KEY (patient_id)
    REFERENCES patients (id);

ALTER TABLE personnels
  ADD CONSTRAINT FK_services_TO_personnels
    FOREIGN KEY (service_id)
    REFERENCES services (id);

ALTER TABLE account_entries
  ADD CONSTRAINT FK_accounts_TO_account_entries
    FOREIGN KEY (account_id)
    REFERENCES accounts (id);

ALTER TABLE account_transfers
  ADD CONSTRAINT FK_accounts_TO_account_transfers
    FOREIGN KEY (to_account_id)
    REFERENCES accounts (id);

ALTER TABLE account_transfers
  ADD CONSTRAINT FK_accounts_TO_account_transfers1
    FOREIGN KEY (from_account_id)
    REFERENCES accounts (id);

ALTER TABLE appointments
  ADD CONSTRAINT FK_patients_TO_appointments
    FOREIGN KEY (patient_id)
    REFERENCES patients (id);

ALTER TABLE services
  ADD CONSTRAINT FK_service_items_TO_services
    FOREIGN KEY (service_item_id)
    REFERENCES service_items (id);

ALTER TABLE customers
  ADD CONSTRAINT FK_patients_TO_customers
    FOREIGN KEY (patient_id)
    REFERENCES patients (id);

ALTER TABLE customers
  ADD CONSTRAINT FK_users_TO_customers
    FOREIGN KEY (user_id)
    REFERENCES users (id);

ALTER TABLE insurances
  ADD CONSTRAINT FK_patients_TO_insurances
    FOREIGN KEY (patient_id)
    REFERENCES patients (id);

ALTER TABLE customers
  ADD CONSTRAINT FK_accounts_TO_customers
    FOREIGN KEY (account_id)
    REFERENCES accounts (id);

ALTER TABLE services
  ADD CONSTRAINT FK_appointments_TO_services
    FOREIGN KEY (appointment_id)
    REFERENCES appointments (id);

ALTER TABLE personnels
  ADD CONSTRAINT FK_users_TO_personnels
    FOREIGN KEY (user_id)
    REFERENCES users (id);

ALTER TABLE users
  ADD CONSTRAINT FK_contacts_TO_users
    FOREIGN KEY (contact_id)
    REFERENCES contacts (id);














        
      