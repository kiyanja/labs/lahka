// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.25.0
// source: contacts.sql

package db

import (
	"context"
	"database/sql"
)

const createContact = `-- name: CreateContact :one
INSERT INTO contacts (
    email,
    phone,
    street,
    postbox
) VALUES (
    $1, $2, $3, $4
) RETURNING id, email, phone, street, postbox
`

type CreateContactParams struct {
	Email   string         `json:"email"`
	Phone   int64          `json:"phone"`
	Street  sql.NullString `json:"street"`
	Postbox string         `json:"postbox"`
}

func (q *Queries) CreateContact(ctx context.Context, arg CreateContactParams) (Contacts, error) {
	row := q.db.QueryRowContext(ctx, createContact,
		arg.Email,
		arg.Phone,
		arg.Street,
		arg.Postbox,
	)
	var i Contacts
	err := row.Scan(
		&i.ID,
		&i.Email,
		&i.Phone,
		&i.Street,
		&i.Postbox,
	)
	return i, err
}

const deleteContact = `-- name: DeleteContact :exec
DELETE FROM contacts WHERE id = $1
`

func (q *Queries) DeleteContact(ctx context.Context, id int64) error {
	_, err := q.db.ExecContext(ctx, deleteContact, id)
	return err
}

const getContact = `-- name: GetContact :one
SELECT id, email, phone, street, postbox FROM contacts
WHERE id = $1 LIMIT 1
`

func (q *Queries) GetContact(ctx context.Context, id int64) (Contacts, error) {
	row := q.db.QueryRowContext(ctx, getContact, id)
	var i Contacts
	err := row.Scan(
		&i.ID,
		&i.Email,
		&i.Phone,
		&i.Street,
		&i.Postbox,
	)
	return i, err
}

const listContacts = `-- name: ListContacts :many
SELECT id, email, phone, street, postbox FROM contacts
ORDER BY id
LIMIT $1
OFFSET $2
`

type ListContactsParams struct {
	Limit  int64 `json:"limit"`
	Offset int64 `json:"offset"`
}

func (q *Queries) ListContacts(ctx context.Context, arg ListContactsParams) ([]Contacts, error) {
	rows, err := q.db.QueryContext(ctx, listContacts, arg.Limit, arg.Offset)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var items []Contacts
	for rows.Next() {
		var i Contacts
		if err := rows.Scan(
			&i.ID,
			&i.Email,
			&i.Phone,
			&i.Street,
			&i.Postbox,
		); err != nil {
			return nil, err
		}
		items = append(items, i)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}

const updateContact = `-- name: UpdateContact :exec
UPDATE contacts 
SET email = $2
WHERE id = $1
`

type UpdateContactParams struct {
	ID    int64  `json:"id"`
	Email string `json:"email"`
}

func (q *Queries) UpdateContact(ctx context.Context, arg UpdateContactParams) error {
	_, err := q.db.ExecContext(ctx, updateContact, arg.ID, arg.Email)
	return err
}
