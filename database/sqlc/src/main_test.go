package db

import (
	"database/sql"
	"testing"
)

const (
	dbDriver = "postgres"
	dbSource = "postgresql://root:secret@localhost:5432/lahkadb?sslmode=disable"
)

var testQueries *Queries
func TestMain(m * testing.M) {
	conn, err := sql.Open(dbDriver, dbSource)
}