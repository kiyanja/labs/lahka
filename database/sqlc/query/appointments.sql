-- name: CreateAppointment :one
INSERT INTO appointments (
    title,
    schedule,
    status,
    patient_id
) VALUES (
    $1, $2, $3, $4
) RETURNING *;  

-- name: GetAppointment :one
SELECT * FROM appointments
WHERE id = $1 LIMIT 1;

-- name: ListAppointments :many
SELECT * FROM appointments
ORDER BY id
LIMIT $1
OFFSET $2;

-- name: UpdateAppointment :exec
UPDATE appointments 
SET title = $2
WHERE id = $1;

-- name: DeleteAppointment :exec
DELETE FROM appointments WHERE id = $1;
