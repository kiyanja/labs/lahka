-- name: CreatePatient_record :one
INSERT INTO patient_records (
    title,
    anamnesis,
    patient_id
) VALUES (
    $1, $2, $3
) RETURNING *;

-- name: GetPatient_record :one
SELECT * FROM patient_records
WHERE id = $1 LIMIT 1;

-- name: ListPatient_records :many
SELECT * FROM patient_records
ORDER BY id
LIMIT $1
OFFSET $2;


-- name: UpdatePatient_record :exec
UPDATE patient_records
SET title = $2
WHERE id = sqlc.arg(id);

-- name: DeletePatient_record :exec
DELETE FROM patient_records WHERE id = $1;


