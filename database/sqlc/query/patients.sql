-- name: CreatePatient :one
INSERT INTO patients (
    code,
    name,
    gender,
    birthday,
    email,
    phone,
    country_code
) VALUES (
    $1, $2, $3, $4, $5, $6, $7
) RETURNING *;

-- name: GetPatient :one
SELECT * FROM patients
WHERE id = $1 LIMIT 1;

-- name: ListPatients :many
SELECT * FROM patients
ORDER BY id
LIMIT $1
OFFSET $2;

-- name: UpdatePatient :exec
UPDATE patients 
SET name = $2
WHERE id = $1;

-- name: DeletePatient :exec
DELETE FROM patients WHERE id = $1;