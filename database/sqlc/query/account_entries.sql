-- name: CreateAccount_entry :one
INSERT INTO account_entries (
    account_id,
    amount
) VALUES (
    $1, $2
) RETURNING *;

-- name: GetAccount_entry :one
SELECT * FROM account_entries
WHERE id = $1 LIMIT 1;

-- name: ListAccount_entries :many
SELECT * FROM account_entries
WHERE account_id = $1
ORDER BY id
LIMIT $2
OFFSET $3;

-- name: UpdateAccount_entry :exec
UPDATE account_entries 
SET amount = $2
WHERE id = $1;

-- name: DeleteAccount_entry :exec
DELETE FROM accounts WHERE id = $1;
