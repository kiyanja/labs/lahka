-- name: CreateAccount_transfer :one
INSERT INTO account_transfers (
    from_account_id,
    amount,
    to_account_id
) VALUES (
    $1, $2, $3
) RETURNING *;

-- name: GetAccount_transfer :one
SELECT * FROM account_transfers
WHERE id = $1 LIMIT 1;

-- name: ListAccount_transfers :many
SELECT * FROM account_transfers
WHERE
    from_account_id = $1 OR to_account_id = $2
ORDER BY id
LIMIT $3
OFFSET $4;

/*
-- name: UpdateAccount_transfer :exec
UPDATE account_transfer 
SET amount = $2
WHERE id = $1;

-- name: DeleteAccount_transfer :exec
DELETE FROM accounts_transfer WHERE id = $1;
*/