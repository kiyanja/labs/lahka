-- CreateInsurance :one
INSERT INTO insurances  (
    name, 
    code,
    patient_id
) VALUES (
    $1, $2, $3
) RETURNING *;

-- GetInsurance :one
SELECT * FROM insurances
WHERE id = $1 LIMIT $1;

-- ListInsurances :many
SELECT * FROM insurances
ORDER BY id
LIMIT $1
OFFSET $2;

-- UpdateInsurance :exec
UPDATE insurances 
SET title = $2
WHERE id = $1;

-- DeleteInsurance :exec
DELETE FROM insurances WHERE id = $1;