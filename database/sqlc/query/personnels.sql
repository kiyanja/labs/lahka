-- name: CreatePersonnel :one
INSERT INTO personnels (
    name,
    email,
    profession,
    department,
    service_id,
    user_id
) VALUES (
    $1, $2, $3, $4, $5, $6
) RETURNING *;

-- name: GetPersonnel :one
SELECT * FROM personnels
WHERE id = $1 LIMIT 1;

-- name: ListPersonnels :many
SELECT * FROM personnels
ORDER BY id
LIMIT $1
OFFSET $2;

-- name: UpdatePersonnel :exec
UPDATE personnels 
SET email = $2
WHERE id = $1;

-- name: DeletePersonnel :exec
DELETE FROM personnels WHERE id = $1;