-- name: CreateService :one
INSERT INTO services (
    title,
    detail,
    service_item_id,
    appointment_id
) VALUES (
    $1, $2, $3, $4
) RETURNING *;

-- name: GetService :one
SELECT * FROM services
WHERE id = $1 LIMIT 1;

-- name: ListServices :many
SELECT * FROM services
ORDER BY id
LIMIT $1
OFFSET $2;

-- name: UpdateService :exec
UPDATE services 
SET detail = $2
WHERE id = $1;

-- name: DeleteService :exec
DELETE FROM services WHERE id = $1;