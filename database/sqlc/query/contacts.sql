-- name: CreateContact :one
INSERT INTO contacts (
    email,
    phone,
    street,
    postbox
) VALUES (
    $1, $2, $3, $4
) RETURNING *;

-- name: GetContact :one
SELECT * FROM contacts
WHERE id = $1 LIMIT 1;

-- name: ListContacts :many
SELECT * FROM contacts
ORDER BY id
LIMIT $1
OFFSET $2;

-- name: UpdateContact :exec
UPDATE contacts 
SET email = $2
WHERE id = $1;

-- name: DeleteContact :exec
DELETE FROM contacts WHERE id = $1;