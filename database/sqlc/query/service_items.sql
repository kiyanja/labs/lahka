-- name: CreateService_item :one
INSERT INTO service_items (
    name,
    code,
    price
) VALUES (
    $1, $2, $3
) RETURNING *;

-- name: GetService_item :one
SELECT * FROM service_items
WHERE id = $1 LIMIT 1;

-- name: ListService_items :many
SELECT * FROM service_items
ORDER BY id
LIMIT $1
OFFSET $2;

-- name: UpdateService_item :exec
UPDATE service_items 
SET price = $2
WHERE id = $1;

-- name: DeleteService_item :exec
DELETE FROM service_items WHERE id = $1;