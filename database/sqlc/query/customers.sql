-- name: CreateCustomer :one
INSERT INTO customers (
    name,
    patient_id,
    user_id,
    account_id
) VALUES (
    $1, $2, $3, $4
) RETURNING *;

-- name: GetCustomer :one
SELECT * FROM customers
WHERE id = $1 LIMIT 1;

-- name: ListCustomers :many
SELECT * FROM customers
ORDER BY id
LIMIT $1
OFFSET $2;

-- name: UpdateCustomer :exec
UPDATE customers 
SET name = $2
WHERE id = $1;

-- name: DeleteCustomer :exec
DELETE FROM customers WHERE id = $1;